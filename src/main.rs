#![feature(try_trait_v2)]

use std::convert::Infallible;
use crate::Command::*;
use std::fs::{File, OpenOptions};
use std::io::{BufRead, BufReader, BufWriter, Seek, Write, Result};
use std::ops::{ControlFlow, FromResidual, Try};
use std::process;
use crate::ExecutionResult::{ExecutionError, ExecutionOk};

fn main() {
    let mut data = RustedData {
        buffer: String::new(),
        file: None,
    };
    while parse_command(&mut data).can_continue() {}
}

struct RustedData {
    buffer: String,
    file: Option<File>,
}

#[derive(Debug, PartialEq)]
enum Command {
    SET,
    SHOW,
    CLEAR,
    EXIT,
    READ,
    WRITE,
}

struct ExecutableCommand {
    command: Command,
    arguments: String,
}

enum ExecutionResult {
    ExecutionOk,
    ExecutionError(bool, String) // bool is true for fatal error, false for non fatal
}

impl ExecutionResult {
    fn can_continue(&self) -> bool {
        match self {
            ExecutionOk => true,
            ExecutionError(fatal, _) => !fatal
        }
    }
}

impl Try for ExecutionResult {
    type Output = Self;
    type Residual = (bool, String);

    fn from_output(output: Self::Output) -> Self {
        output
    }

    fn branch(self) -> ControlFlow<Self::Residual, Self::Output> {
        match self {
            ExecutionOk => ControlFlow::Continue(ExecutionOk),
            ExecutionError(fatal, message) => ControlFlow::Break((fatal, message))
        }
    }
}

impl FromResidual for ExecutionResult {
    fn from_residual(residual: <Self as Try>::Residual) -> Self {
        ExecutionError(residual.0, residual.1)
    }
}

impl FromResidual<Result<Infallible>> for ExecutionResult {
    fn from_residual(residual: Result<Infallible>) -> Self {
        match residual {
            Ok(_) => ExecutionOk,
            Err(message) => ExecutionError(true, message.to_string())
        }
    }
}

impl ExecutableCommand {
    fn from_string(command: &str, argument: &str) -> Option<ExecutableCommand> {
        match command {
            "set" => Some(ExecutableCommand{command: SET, arguments: argument.to_string()}),
            "show" => Some(ExecutableCommand{command: SHOW, arguments: argument.to_string()}),
            "clear" => Some(ExecutableCommand{command: CLEAR, arguments: argument.to_string()}),
            "exit" => Some(ExecutableCommand{command: EXIT, arguments: argument.to_string()}),
            "read" => Some(ExecutableCommand{command: READ, arguments: argument.to_string()}),
            "write" => Some(ExecutableCommand{command: WRITE, arguments: argument.to_string()}),
            _ => None,
        }
    }

    fn _to_string(&self) -> String {
        match self.command {
            SET => "set".to_string(),
            SHOW => "show".to_string(),
            CLEAR => "clear".to_string(),
            EXIT => "exit".to_string(),
            READ => "read".to_string(),
            WRITE => "write".to_string(),
        }
    }

    fn from_option(contents: Option<(&str, &str)>) -> Option<ExecutableCommand> {
        match contents {
            Some((command, arguments)) => ExecutableCommand::from_string(command, arguments),
            None => None,
        }
    }

    fn execute(&self, data: &mut RustedData) -> ExecutionResult {
        match self.command {
            SET => {
                if self.arguments.len() == 0 {
                    println!("< \"set\" requires an argument. To clear the buffer use \"clear\"");
                } else {
                    data.buffer.clear();
                    data.buffer.push_str(self.arguments.as_str());
                }
            },
            SHOW => {
                println!("< {}", data.buffer);
            },
            CLEAR => {
                data.buffer.clear();
            },
            EXIT => {
                exit();
            },
            READ => {
                setup_file(self.arguments.clone(), data, false, "read")?;
                let mut buffer = BufReader::new(data.file.as_mut().unwrap());
                data.buffer.clear();
                buffer.read_line(&mut data.buffer)?;
            },
            WRITE => {
                    setup_file(self.arguments.clone(), data, true,"write")?;
                    let mut buffer = BufWriter::new(data.file.as_mut().unwrap());
                    buffer.write(data.buffer.as_bytes())?;
                    buffer.flush()?;
            },
        }
        ExecutionOk
    }
}

fn parse_command(data: &mut RustedData) -> ExecutionResult {
    print!("> ");
    std::io::stdout().flush().expect("Could not flush prompt");
    let mut input = String::new();
    match std::io::stdin().read_line(&mut input) {
        Ok(0) => exit(), // EOF
        Ok(_) => {}
        Err(e) => {
            println!("Failed to read user input");
            return ExecutionError(true, "Failed to read user input".to_string());
        }
    }

    let trimmed_input = input.trim_start().trim_end_matches('\n');
    let command;

    if trimmed_input.contains(" ") {
        command = ExecutableCommand::from_option(trimmed_input.split_once(' '));
    } else {
        command = ExecutableCommand::from_string(trimmed_input.trim(), "");
    }

    match command {
        None => {
            println!("Unknown command {}", input);
        }
        Some(cmd) => {
            cmd.execute(data)?;
        }
    }
    ExecutionOk
}

fn exit() {
    process::exit(0);
}

/// Checks a file operation against the current application state and ensures that the requested
/// path checks out and then updates the application state so the caller can proceed.
///
/// # Return value
/// Returns Ok(true) if the caller can continue with their operation
///
/// Returns Ok(false) if user error was detected, like the path was incorrect or argument is required
///
/// Returns Err() on I/O error which is generally fatal
fn setup_file(path: String, data: &mut RustedData, allow_create: bool, operation_name: &str) -> ExecutionResult {
    /*
     * If the user provided no argument for this invocation [1], then we need to check if they
     * did so in the past [2].
     *      If not, then that's an error
     *      If yes, then we use that
     * If the user did provide an argument, then we try to use it. [3]
     *      If successful, then we replace the old one [4]
     *      If unsuccessful, then that's an error [5]
     */
    if path.len() == 0 {
        // [1]
        if data.file.is_none() {
            // [2]
            println!("< \"{}\" requires a file argument.", operation_name);
        } else {
            data.file.as_mut().unwrap().rewind()?;
        }
    } else {
        // [3]
        match OpenOptions::new()
            .read(true)
            .write(true)
            .create(allow_create)
            .open(path.as_str())
        {
            Ok(file) => {
                // [4]
                data.file = Some(file);
            }
            Err(e) => {
                // [5]
                let message = format!("Error opening file {path} : {e}");
                return ExecutionError(true, message);
            }
        }
    }
    ExecutionOk
}
