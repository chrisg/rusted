# Rusted
## The world's worst text editor

This is rusted, a very simple Rust based text editor. It is being developed in a series of videos that can be found here:

https://www.youtube.com/@codetales

It is intended to be an educational exercise as an extensible rust project, with emphasis on I/O.
All the design choices are discussed in the video series.

The project can be checked out and run with `cargo run` provided you have a proper rust toolchain installed.

### License
This software is distributed under the Anti Capitalist Software License v1.4 as described [here](https://anticapitalist.software/). The text of the license and copyright information can be found in LICENSE.txt